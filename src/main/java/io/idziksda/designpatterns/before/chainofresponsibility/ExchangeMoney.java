package io.idziksda.designpatterns.before.chainofresponsibility;

public class ExchangeMoney {
    private double value;
    private String currency;

    public ExchangeMoney(double sum, String currency) {
        this.value = sum;
        this.currency = currency;
    }

    public double exchangeMoney() {
        switch (currency) {
            case "DOLLAR":
                return value / 4.13;
            case "PLN":
                return value * 4.13;
            case "EURO":
                return value / 4.02;
        }
        return value*4.13;
    }
}

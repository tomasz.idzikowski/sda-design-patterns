package io.idziksda.designpatterns.before.chainofresponsibility;

public class Main {
    public static void main(String[] args) {
        ExchangeMoney change = new ExchangeMoney(134,"PLN");
        System.out.println(change.exchangeMoney());
    }
}

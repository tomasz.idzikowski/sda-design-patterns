package io.idziksda.designpatterns.before.singleton;

public class Main {
    public static void main(String[] args) {
        Civilization civilization1=new Civilization();
        Civilization civilization2=new Civilization();
        System.out.println(civilization1.equals(civilization2));
    }
}

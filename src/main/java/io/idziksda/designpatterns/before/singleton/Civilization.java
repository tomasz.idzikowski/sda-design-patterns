package io.idziksda.designpatterns.before.singleton;

import java.util.List;

public class Civilization {
    private int MAX_UNITS_NUMBER = 100;
    private List<String> availableUnits;
    private List<String> availableTerrains;

    public void runGame(){
        // game mechanism
    }
}

package io.idziksda.designpatterns.before.builder;

public class Main {
    public static void main(String[] args) {
        Computer computer= new Computer("Intel i5-10400F","16 GB (2x8GB)","GeForce GTX 1650");
        System.out.println(computer);
    }
}

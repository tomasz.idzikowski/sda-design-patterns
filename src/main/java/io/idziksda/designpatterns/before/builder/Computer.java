package io.idziksda.designpatterns.before.builder;

public class Computer {
    private String processor;
    private String memory;
    private String graphicCard;

    public Computer(String processor, String memory, String graphicCard) {
        this.processor = processor;
        this.memory = memory;
        this.graphicCard = graphicCard;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getGraphicCard() {
        return graphicCard;
    }

    public void setGraphicCard(String graphicCard) {
        this.graphicCard = graphicCard;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "processor='" + processor + '\'' +
                ", memory='" + memory + '\'' +
                ", graphicCard='" + graphicCard + '\'' +
                '}';
    }
}

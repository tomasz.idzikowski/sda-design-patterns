package io.idziksda.designpatterns.before.strategy;

public class Main {
    public static void main(String[] args) {
        Boxer boxer = new Boxer("boxer");
        boxer.leftPunch();
        boxer.rightPunch();
    }
}
//a co będzie gdy będzie musiał uderzyć inny sposobem
//czy będziemy modyfikowac ciągle klase Boxer?

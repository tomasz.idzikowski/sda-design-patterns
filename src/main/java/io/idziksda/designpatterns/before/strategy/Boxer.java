package io.idziksda.designpatterns.before.strategy;

public class Boxer {
    private String name;

    public Boxer(String name) {
        this.name = name;
    }

    public void leftPunch(){
        System.out.println("Hiting with left punch");
    }

    public void rightPunch(){
        System.out.println("Hiting with rigght punch");
    }
}

package io.idziksda.designpatterns.before.flyweight;

public class BlackDodge {
    private int mileage;
    private int red;
    private int green;
    private int black;

    public BlackDodge(int mileage, int red, int green, int black) {
        this.mileage = mileage;
        this.red = red;
        this.green = green;
        this.black = black;
    }
}

package io.idziksda.designpatterns.before.flyweight;

public class YellowFord {
    private int mileage;
    private int red;
    private int green;
    private int black;

    public YellowFord(int mileage, int red, int green, int black) {
        this.mileage = mileage;
        this.red = red;
        this.green = green;
        this.black = black;
    }
}

package io.idziksda.designpatterns.before.adapter;

public class OldApplication {
    public void use(LegacyApi api){
        api.set();
    }
}

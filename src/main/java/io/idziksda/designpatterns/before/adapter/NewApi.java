package io.idziksda.designpatterns.before.adapter;

public interface NewApi {
    void set();
}

package io.idziksda.designpatterns.before.adapter;

public class NewApiImpl implements NewApi{
    @Override
    public void set() {
        System.out.println("New Api is set");
    }
}

package io.idziksda.designpatterns.before.adapter;

public class LegacyApiImpl implements LegacyApi{
    @Override
    public void set() {
        System.out.println("Legacy Api is set");
    }
}

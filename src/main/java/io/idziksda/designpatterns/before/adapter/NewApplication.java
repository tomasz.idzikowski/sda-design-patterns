package io.idziksda.designpatterns.before.adapter;

public class NewApplication {
    public void use(NewApi api){
        api.set();
    }
}

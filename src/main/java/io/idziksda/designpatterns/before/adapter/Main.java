package io.idziksda.designpatterns.before.adapter;

public class Main {
    public static void main(String[] args) {
        LegacyApi legacyApi=new LegacyApiImpl();
        OldApplication oldApplication=new OldApplication();
        oldApplication.use(legacyApi);

        NewApi newApi=new NewApiImpl();
        NewApplication newApplication =new NewApplication();
        newApplication.use(newApi);

//        oldApplication.use(newApi);
    }
}

// a co jesli chcemy by stara aplikacja korzystała z nowego api?

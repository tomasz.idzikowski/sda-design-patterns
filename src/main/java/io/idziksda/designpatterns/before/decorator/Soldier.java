package io.idziksda.designpatterns.before.decorator;

public class Soldier {
    private String name;
    private int hp;

    public Soldier(String name, int hp) {
        this.name = name;
        this.hp = hp;
    }
}

package io.idziksda.designpatterns.before.decorator;

public class MediKit extends Soldier{
    public MediKit() {
        super("Soldier with medi-kit", 10);
    }
}

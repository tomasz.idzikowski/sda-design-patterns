package io.idziksda.designpatterns.before.decorator;

public class PersonalArmour extends Soldier{
    public PersonalArmour() {
        super("Soldier with armour", 20);
    }
}

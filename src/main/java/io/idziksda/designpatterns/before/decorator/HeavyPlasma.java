package io.idziksda.designpatterns.before.decorator;

public class HeavyPlasma extends Soldier{
    public HeavyPlasma() {
        super("Soldier with Heavy Plasma", 30);
    }
}

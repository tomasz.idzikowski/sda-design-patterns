package io.idziksda.designpatterns.before.decorator;

public class Main {
    public static void main(String[] args) {
        Soldier soldier = new Soldier("Matthias",10);
        Soldier soldier1 = new PersonalArmour();
        Soldier soldier2 = new HeavyPlasma();
        Soldier soldier3= new MediKit();
    }
}
// aby dodać żólnierzowi kolejne rzeczy aby zwiekszyć jego hp
// musimy dodawać kolejne klasy

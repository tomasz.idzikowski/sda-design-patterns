package io.idziksda.designpatterns.before.visitor;

public class Tank {
    private String name;
    private int armour;
    private String weapon;

    public Tank(String name, int armour, String weapon) {
        this.name = name;
        this.armour = armour;
        this.weapon = weapon;
    }

    public String getName() {
        return name;
    }

    public String getWeapon() {
        return weapon;
    }

    public int getArmour() {
        return armour;
    }

    public int getPower() {
        if (getWeapon().equals("Heavy Plasma")) {
            return getArmour() * 3;
        }
        return getArmour() * 4;
    }
}

package io.idziksda.designpatterns.before.visitor;

public class F16 {
    private int armor;
    private String weapon;

    public F16(int armor, String weapon) {
        this.armor = armor;
        this.weapon = weapon;
    }

    public int getArmor() {
        return armor;
    }

    public String getWeapon() {
        return weapon;
    }

    public int getPower() {
        if (getWeapon().equals("Heavy Plasma")) {
            return getArmor()+30;
        }
        return getArmor()+50;
    }
}

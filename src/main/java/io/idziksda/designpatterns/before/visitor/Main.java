package io.idziksda.designpatterns.before.visitor;

public class Main {
    public static void main(String[] args) {
        Soldier soldier = new Soldier("Terminator", 50, "Laser Pistol");
        Tank tank = new Tank("Rudy-102", 300, "Plasma Beam");
        F16 f16 = new F16(250, "Fusion Ball");

        System.out.println(soldier.getPower());
        System.out.println(tank.getWeapon());
        System.out.println(f16.getWeapon());
    }
}

//a co gdyby zwykły obywatel zaczał szacować moc bojową jednostek?

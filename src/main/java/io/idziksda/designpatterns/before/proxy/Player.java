package io.idziksda.designpatterns.before.proxy;

public class Player {
    private String name;
    private String nationality;
    private int value;

    public Player(String name, String nationality, int value) {
        this.name = name;
        this.nationality = nationality;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getNationality() {
        return nationality;
    }

    public int getValue() {
        return value;
    }
}

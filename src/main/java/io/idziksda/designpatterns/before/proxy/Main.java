package io.idziksda.designpatterns.before.proxy;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Team bayernTeam=new Team();
        bayernTeam.getPlayerValues(0);
        bayernTeam.getPlayerValues(1);
        bayernTeam.getPlayerValues(2);
    }
}

package io.idziksda.designpatterns.before.proxy;

import java.util.Arrays;
import java.util.List;

public class Team {
    private List<Player> players;

    private int loadPlayerValue(int index) {
        return players.get(index).getValue();
    }

    private void loadPlayers() throws InterruptedException {
        Player player1=new Player("Lewandowski","Polish",40);
        Player player2=new Player("Muller","German",30);
        Player player3=new Player("Sane","Senegal",60);

        this.players= Arrays.asList(player1,player2,player3);
        Thread.sleep(5000);
    }

    public void getPlayerValues(int index) throws InterruptedException {
        loadPlayers();
        System.out.println(loadPlayerValue(index));
    }
}

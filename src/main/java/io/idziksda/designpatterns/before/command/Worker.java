package io.idziksda.designpatterns.before.command;

public class Worker {
    public void buildFarm() {
        System.out.println("Building Farm");
    }

    public void buildMine() {
        System.out.println("Building mine");
    }

    public void buildIrrigation() {
        System.out.println("Building irrigation");
    }
}

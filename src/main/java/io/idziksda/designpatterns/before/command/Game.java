package io.idziksda.designpatterns.before.command;

public class Game {
    private Worker worker;

    public Game(Worker worker) {
        this.worker = worker;
    }

    public void executeWorkerCommands(){
        worker.buildFarm();
        worker.buildMine();
        worker.buildIrrigation();
    }
}

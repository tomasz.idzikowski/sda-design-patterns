package io.idziksda.designpatterns.before.command;

public class Main {
    public static void main(String[] args) {
        Worker worker = new Worker();
        Game game= new Game(worker);
        game.executeWorkerCommands();
    }
}

//a co jesli chcemy zmienić kolejnośc wywoływanych metod
//dodać kolejkę
//dodać nową jednostkę

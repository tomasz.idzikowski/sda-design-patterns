package io.idziksda.designpatterns.before.templatemethod;

public class ApplicationLinuxBuilder {
    public void writeApp(){
        openSystem();
        installIDE();
        runIDE();
    }

    private void openSystem(){
        System.out.println("System opened");
    }

    private void installIDE(){
        System.out.println("IDE installed");
    }

    private void runIDE(){
        System.out.println("IDE ran");
    }
}

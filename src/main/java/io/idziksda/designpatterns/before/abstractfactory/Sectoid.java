package io.idziksda.designpatterns.before.abstractfactory;

public class Sectoid extends Alien {
    public Sectoid(String rank, int stamina) {
        super(rank, stamina);
    }
}

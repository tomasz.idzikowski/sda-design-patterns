package io.idziksda.designpatterns.before.abstractfactory;

public class Muton extends Alien {

    public Muton(String rank, int stamina) {
        super(rank, stamina);
    }
}

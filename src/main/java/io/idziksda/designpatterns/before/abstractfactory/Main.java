package io.idziksda.designpatterns.before.abstractfactory;

public class Main {
    public static void main(String[] args) {
        Alien sectoid = new Sectoid("leader", 150);
        Alien muton = new Muton("soldier", 100);
    }
}

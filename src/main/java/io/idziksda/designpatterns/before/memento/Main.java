package io.idziksda.designpatterns.before.memento;

public class Main {
    public static void main(String[] args) {
        Player player1 = new Player("Lewandowski",40,38,10);
        Player player2 = new Player("Lewandowski",400,380,100);
    }
}

//praktycznie nie ma jak zapisywać wersji po liczbie zapisanej w respekt
//trzeba by było pamiętać pozostałe wartości

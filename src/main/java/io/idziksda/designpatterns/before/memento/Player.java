package io.idziksda.designpatterns.before.memento;

public class Player {
    private String name;
    private int matches;
    private int goals;
    private int respect;

    public Player(String name, int matches, int goals, int respect) {
        this.name = name;
        this.matches = matches;
        this.goals = goals;
        this.respect = respect;
    }

    @Override
    public String toString() {
        return "PlayerOriginator{" +
                "name='" + name + '\'' +
                ", matches=" + matches +
                ", goals=" + goals +
                ", respect=" + respect +
                '}';
    }
}

package io.idziksda.designpatterns.before.observer;

public class TvNotificaton {
    public void updatePlayerStatus(Player player) {
        System.out.println("Tv info : ");
        System.out.println("PlayerOriginator " + player + " changed status to " + player.getStatus());
    }
}

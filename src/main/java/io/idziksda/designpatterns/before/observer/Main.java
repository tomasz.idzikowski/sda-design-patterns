package io.idziksda.designpatterns.before.observer;

public class Main {
    public static void main(String[] args) {
        Player player=new Player("Lewandowski",Status.IDLE);
        TvNotificaton tv=new TvNotificaton();
        RadioNotification radio=new RadioNotification();
        player.setStatus(Status.SCORED);
        tv.updatePlayerStatus(player);
        radio.updatePlayerStatus(player);
    }
}

// powtorzone te same metody
// po kazdym zmianie statusu wywoływanie metody
// kolejny notification - bałagan

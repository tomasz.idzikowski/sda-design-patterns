package io.idziksda.designpatterns.before.observer;

public class RadioNotification {
    public void updatePlayerStatus(Player player) {
        System.out.println("Radio info : ");
        System.out.println("PlayerOriginator " + player + " changed status to " + player.getStatus());
    }
}

package io.idziksda.designpatterns.before.state;

public enum State {
    ENGINE_NOT_STARTED,
    ENGINE_STARTED,
    MISSLE_LOADED,
    TARGET_LOCKED,
    FIRED
}

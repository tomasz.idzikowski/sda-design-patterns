package io.idziksda.designpatterns.before.state;

public class Main {
    public static void main(String[] args) {
        Tank tank = new Tank();
        tank.fire();
        tank.getOnTarget();
        tank.loadMissle();
        tank.startTheEngine();
    }
}

//dodawanie kolejne stanu wprowadza ogromnie zamieszanie w klasie tank

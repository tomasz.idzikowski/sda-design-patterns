package io.idziksda.designpatterns.before.state;

public class Tank {
    private State state;

    public Tank() {
        this.state = State.ENGINE_NOT_STARTED;
    }

    public void startTheEngine() {
        switch (state) {
            case ENGINE_NOT_STARTED:
                state = State.ENGINE_STARTED;
                System.out.println("Engine started");
                break;
            default:
                System.out.println("Engine already started");
        }
    }

    public void loadMissle() {
        switch (state) {
            case ENGINE_NOT_STARTED:
                System.out.println("You have to start the engine first");
                break;
            case ENGINE_STARTED:
                state = State.MISSLE_LOADED;
                System.out.println("Missle loaded");
                break;
            default:
                System.out.println("Missle already loaded");
        }
    }

    public void getOnTarget() {
        switch (state) {
            case ENGINE_NOT_STARTED:
            case ENGINE_STARTED:
                System.out.println("You have to load the missle first");
                break;
            case MISSLE_LOADED:
                state = State.TARGET_LOCKED;
                System.out.println("The target locked");
                break;
            default:
                System.out.println("The target already locked");
        }
    }

    public void fire() {
        switch (state) {
            case ENGINE_NOT_STARTED:
            case ENGINE_STARTED:
            case MISSLE_LOADED:
                System.out.println("You have to lock the target first");
                break;
            default:
                state = State.FIRED;
                System.out.println("Fired");
        }
    }
}

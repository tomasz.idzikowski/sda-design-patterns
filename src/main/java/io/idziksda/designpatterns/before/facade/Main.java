package io.idziksda.designpatterns.before.facade;

public class Main {
    private static void checkLoad(String command) {
        ComputerCPU cpu = new ComputerCPU();
        ComputerManager manager = new ComputerManager();

        if (manager.runSystem()) {
            cpu.runCommand(command);
            cpu.showLoad();
        }
    }

    public static void main(String[] args) {
        checkLoad("./start.sh");
    }
}

//nie chcemy by klient miał dostęp bezpośredni dostęp
// metod klas ComputerCPU oraz ComputerManager

package io.idziksda.designpatterns.before.facade;

public class ComputerManager {
    public boolean runSystem() {
        System.out.println("System started");
        return true;
    }

    public boolean resetSystem() {
        System.out.println("System restarted");
        return true;
    }

    public boolean shutDownSystem() {
        System.out.println("System stopped");
        return true;
    }
}

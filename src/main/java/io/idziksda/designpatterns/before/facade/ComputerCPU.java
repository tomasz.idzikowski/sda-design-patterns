package io.idziksda.designpatterns.before.facade;

public class ComputerCPU {
    public boolean runCommand(String command) {
        System.out.println(command + " executed");
        return true;
    }

    public void showLoad(){
        System.out.println("Current Load is 0.5");
    }
}

package io.idziksda.designpatterns.after.abstractfactory;

import io.idziksda.designpatterns.after.abstractfactory.units.BlueArmyFactory;
import io.idziksda.designpatterns.after.abstractfactory.units.RedArmyFactory;
import io.idziksda.designpatterns.after.abstractfactory.units.UnitFactory;

public class Main {
    public static void main(String[] args) {
        UnitFactory redArmyFactory = new RedArmyFactory();
        UnitFactory blueArmyFactory =new BlueArmyFactory();

        blueArmyFactory.createAndroid("T100");
        blueArmyFactory.createAlien("sectoid");

        redArmyFactory.createAndroid("T800");
        redArmyFactory.createAlien("muton");
    }
}

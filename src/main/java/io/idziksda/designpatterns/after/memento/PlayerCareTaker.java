package io.idziksda.designpatterns.after.memento;

import java.util.ArrayList;
import java.util.List;

public class PlayerCareTaker {
    private List<PlayerMemento> mementoList = new ArrayList<>();

    public String add(PlayerMemento playerMemento) {
        mementoList.add(playerMemento);
        return playerMemento.toString();
    }

    public PlayerMemento get(int index) {
        return mementoList.get(index);
    }
}

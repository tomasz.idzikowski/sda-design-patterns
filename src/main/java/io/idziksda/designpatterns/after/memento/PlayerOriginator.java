package io.idziksda.designpatterns.after.memento;

public class PlayerOriginator {
    private String name;
    private int matches;
    private int goals;
    private int respect;

    public PlayerMemento save() {
        return new PlayerMemento(this.name, this.matches, this.goals, this.respect);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMatches(int matches) {
        this.matches = matches;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    String load(PlayerMemento playerMemento) {
        this.name = playerMemento.getName();
        this.matches = playerMemento.getMatches();
        this.goals = playerMemento.getGoals();
        return this.toString();
    }

    public void setRespect(int respect) {
        this.respect = respect;
    }

    @Override
    public String toString() {
        return "PlayerOriginator{" +
                "name='" + name + '\'' +
                ", matches=" + matches +
                ", goals=" + goals +
                ", respect=" + respect +
                '}';
    }
}

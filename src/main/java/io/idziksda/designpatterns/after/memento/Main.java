package io.idziksda.designpatterns.after.memento;

public class Main {
    public static void main(String[] args) {
        PlayerOriginator originator=new PlayerOriginator();
        originator.setName("Lewandowski");
        originator.setMatches(10);
        originator.setGoals(3);
        originator.setRespect(10);

        PlayerCareTaker taker= new PlayerCareTaker();
        taker.add(originator.save());

        originator.setMatches(100);
        originator.setGoals(80);
        originator.setRespect(50);

        taker.add(originator.save());

        System.out.println(originator.load(taker.get(1)));
    }
}

package io.idziksda.designpatterns.after.memento;

public class PlayerMemento {
    private String name;
    private int matches;
    private int goals;
    private int respect;

    public PlayerMemento(String name, int matches, int goals, int respect) {
        this.name = name;
        this.matches = matches;
        this.goals = goals;
        this.respect = respect;
    }

    public String getName() {
        return name;
    }

    public int getMatches() {
        return matches;
    }

    public int getGoals() {
        return goals;
    }

    public int getRespect() {
        return respect;
    }

    @Override
    public String toString() {
        return "PlayerMemento{" +
                "name='" + name + '\'' +
                ", matches=" + matches +
                ", goals=" + goals +
                ", respect=" + respect +
                '}';
    }
}

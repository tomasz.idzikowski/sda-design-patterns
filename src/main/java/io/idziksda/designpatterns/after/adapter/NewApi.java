package io.idziksda.designpatterns.after.adapter;

public interface NewApi {
    void set();
}

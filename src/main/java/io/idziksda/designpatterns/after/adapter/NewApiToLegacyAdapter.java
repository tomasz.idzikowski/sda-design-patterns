package io.idziksda.designpatterns.after.adapter;

public class NewApiToLegacyAdapter implements LegacyApi{
    private NewApi api;

    public NewApiToLegacyAdapter(NewApi api) {
        this.api = api;
    }

    @Override
    public void set() {
        api.set();
    }
}

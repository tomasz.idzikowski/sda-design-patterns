package io.idziksda.designpatterns.after.adapter;

public class NewApiImpl implements NewApi {
    @Override
    public void set() {
        System.out.println("New Api is set");
    }
}

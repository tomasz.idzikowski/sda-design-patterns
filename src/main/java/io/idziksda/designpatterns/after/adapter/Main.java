package io.idziksda.designpatterns.after.adapter;

public class Main {
    public static void main(String[] args) {
        LegacyApi legacyApi=new LegacyApiImpl();
        OldApplication oldApplication=new OldApplication();
        oldApplication.use(legacyApi);

        NewApi newApi=new NewApiImpl();
        NewApplication newApplication =new NewApplication();
        newApplication.use(newApi);

        NewApiToLegacyAdapter adapter=new NewApiToLegacyAdapter(newApi);
        oldApplication.use(adapter);
    }
}

// a co jesli chcemy by stara aplikacja korzystała z nowego api?

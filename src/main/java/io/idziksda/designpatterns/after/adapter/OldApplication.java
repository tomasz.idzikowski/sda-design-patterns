package io.idziksda.designpatterns.after.adapter;

public class OldApplication {
    public void use(LegacyApi api){
        api.set();
    }
}

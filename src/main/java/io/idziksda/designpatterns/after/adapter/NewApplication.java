package io.idziksda.designpatterns.after.adapter;

public class NewApplication {
    public void use(NewApi api){
        api.set();
    }
}

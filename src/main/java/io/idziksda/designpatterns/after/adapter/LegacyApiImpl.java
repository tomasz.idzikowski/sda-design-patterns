package io.idziksda.designpatterns.after.adapter;

public class LegacyApiImpl implements LegacyApi {
    @Override
    public void set() {
        System.out.println("Legacy Api is set");
    }
}

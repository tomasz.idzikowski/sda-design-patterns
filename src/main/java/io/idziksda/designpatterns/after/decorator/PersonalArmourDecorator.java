package io.idziksda.designpatterns.after.decorator;

public class PersonalArmourDecorator extends SoldierDecorator {
    private final Soldier soldier;

    public PersonalArmourDecorator(Soldier soldier) {
        this.soldier = soldier;
    }

    @Override
    public String getDesc() {
        return soldier.getDesc()+ " personal armour ";
    }

    @Override
    public int getHp() {
        return soldier.getHp()+20;
    }
}

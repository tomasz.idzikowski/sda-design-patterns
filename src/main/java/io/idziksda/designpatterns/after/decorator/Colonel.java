package io.idziksda.designpatterns.after.decorator;

public class Colonel implements Soldier{
    @Override
    public String getDesc() {
        return "Colonel";
    }

    @Override
    public int getHp() {
        return 20;
    }
}

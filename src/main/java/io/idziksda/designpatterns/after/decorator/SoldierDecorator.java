package io.idziksda.designpatterns.after.decorator;

public abstract class SoldierDecorator implements Soldier {
    @Override
    public String getDesc() {
        return "Building soldier";
    }
}

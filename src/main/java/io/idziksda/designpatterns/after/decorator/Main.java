package io.idziksda.designpatterns.after.decorator;

public class Main {
    public static void main(String[] args) {
        Soldier matthias = new MediKitDecorator(
                new HeavyPlasmaDecorator(
                        new PersonalArmourDecorator(
                                new Rookie())));
        System.out.println(matthias.getHp());
        System.out.println(matthias.getDesc());
    }
}

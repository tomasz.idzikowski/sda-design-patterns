package io.idziksda.designpatterns.after.decorator;

public class Rookie implements Soldier{
    @Override
    public String getDesc() {
        return "Rookie";
    }

    @Override
    public int getHp() {
        return 10;
    }
}

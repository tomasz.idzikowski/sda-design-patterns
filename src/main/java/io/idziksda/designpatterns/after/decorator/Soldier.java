package io.idziksda.designpatterns.after.decorator;

public interface Soldier {
    String getDesc();
    int getHp();
}

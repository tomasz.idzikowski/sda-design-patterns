package io.idziksda.designpatterns.after.decorator;

public class MediKitDecorator extends SoldierDecorator{
    private final Soldier soldier;

    public MediKitDecorator(Soldier soldier) {
        this.soldier = soldier;
    }

    @Override
    public String getDesc() {
        return soldier.getDesc()+ " medi kit ";
    }

    @Override
    public int getHp() {
        return soldier.getHp()+30;
    }
}

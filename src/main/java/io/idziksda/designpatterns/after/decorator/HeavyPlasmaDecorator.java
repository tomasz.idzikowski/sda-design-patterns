package io.idziksda.designpatterns.after.decorator;

public class HeavyPlasmaDecorator extends SoldierDecorator {

    private final Soldier soldier;

    public HeavyPlasmaDecorator(Soldier soldier) {
        this.soldier = soldier;
    }

    @Override
    public String getDesc() {
        return soldier.getDesc()+ " heavy plasma ";
    }

    @Override
    public int getHp() {
        return soldier.getHp()+10;
    }
}

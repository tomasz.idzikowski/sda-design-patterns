package io.idziksda.designpatterns.after.templatemethod;

public abstract class ApplicationBuilder {
    public final void writeApp(){
        openSystem();
        installIDE();
        runIDE();
    }

    private void openSystem(){
        System.out.println("System opened");
    }

    protected abstract void installIDE();
    protected abstract void runIDE();
}

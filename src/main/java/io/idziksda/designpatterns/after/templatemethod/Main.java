package io.idziksda.designpatterns.after.templatemethod;

public class Main {
    public static void main(String[] args) {
        ApplicationBuilder appWindows=new ApplicationWindowsBuilder();
        appWindows.writeApp();
    }
}

package io.idziksda.designpatterns.after.templatemethod;

public class ApplicationLinuxBuilder extends ApplicationBuilder{

    @Override
    protected void installIDE() {
        System.out.println("Installing on Linux");
    }

    @Override
    protected void runIDE() {
        System.out.println("Running on Linux");
    }
}

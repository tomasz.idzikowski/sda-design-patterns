package io.idziksda.designpatterns.after.facade.computer;

class ComputerCPU {
    boolean runCommand(String command) {
        System.out.println(command + " executed");
        return true;
    }

    void showLoad(){
        System.out.println("Current Load is 0.5");
    }
}

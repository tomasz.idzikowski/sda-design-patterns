package io.idziksda.designpatterns.after.facade;

import io.idziksda.designpatterns.after.facade.computer.ComputerFacade;

public class Main {

    public static void main(String[] args) {
        ComputerFacade facade=new ComputerFacade();
        facade.checkLoad("./start.sh");
    }
}

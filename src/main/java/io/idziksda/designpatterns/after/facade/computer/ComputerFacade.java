package io.idziksda.designpatterns.after.facade.computer;

public class ComputerFacade {
    private ComputerCPU cpu;
    private ComputerManager manager;

    public ComputerFacade() {
        this.cpu = new ComputerCPU();
        this.manager = new ComputerManager();
    }

    public void checkLoad(String command) {
        if (manager.runSystem()) {
            cpu.runCommand(command);
            cpu.showLoad();
        }
    }
}

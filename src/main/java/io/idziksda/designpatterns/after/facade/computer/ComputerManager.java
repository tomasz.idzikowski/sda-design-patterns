package io.idziksda.designpatterns.after.facade.computer;

class ComputerManager {
    boolean runSystem() {
        System.out.println("System started");
        return true;
    }

   boolean resetSystem() {
        System.out.println("System restarted");
        return true;
    }

   boolean shutDownSystem() {
        System.out.println("System stopped");
        return true;
    }
}

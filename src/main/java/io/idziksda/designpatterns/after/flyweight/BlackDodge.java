package io.idziksda.designpatterns.after.flyweight;

public class BlackDodge extends Car{
    private int mileage;

    public BlackDodge(int mileage) {
        super(mileage, "black");
    }
}

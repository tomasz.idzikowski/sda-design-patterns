package io.idziksda.designpatterns.after.flyweight;

public class YellowFord extends Car{
    private int mileage;

    public YellowFord(int mileage, String color) {
        super(mileage, "yellow");
    }
}

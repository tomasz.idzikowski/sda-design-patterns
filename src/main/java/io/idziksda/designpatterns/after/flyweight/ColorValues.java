package io.idziksda.designpatterns.after.flyweight;

public class ColorValues {
    private static Color white=new Color(255,255,255);
    private static Color black=new Color(0,0,0);
    private static Color yellow=new Color(255,255,0);
    private static Color red=new Color(255,0,0);
    private static Color blue=new Color(0,0,255);
    private static Color green=new Color(0,255,0);

    public static Color getWhite() {
        return white;
    }

    public static Color getBlack() {
        return black;
    }

    public static Color getYellow() {
        return yellow;
    }

    public static Color getRed() {
        return red;
    }

    public static Color getBlue() {
        return blue;
    }

    public static Color getGreen() {
        return green;
    }
}

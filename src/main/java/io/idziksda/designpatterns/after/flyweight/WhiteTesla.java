package io.idziksda.designpatterns.after.flyweight;

public class WhiteTesla extends Car{
    private int mileage;

    public WhiteTesla(int mileage, String color) {
        super(mileage, "white");
    }
}

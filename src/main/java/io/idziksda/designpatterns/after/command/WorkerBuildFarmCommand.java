package io.idziksda.designpatterns.after.command;

public class WorkerBuildFarmCommand implements Command{
    private Worker worker;

    public WorkerBuildFarmCommand(Worker worker) {
        this.worker = worker;
    }

    @Override
    public void execute() {
        worker.buildFarm();
    }

    @Override
    public void undo() {
        worker.undoBuildFarm();
    }
}

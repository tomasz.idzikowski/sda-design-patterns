package io.idziksda.designpatterns.after.command;

public interface Command {
    void execute();
    void undo();
}

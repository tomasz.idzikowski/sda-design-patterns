package io.idziksda.designpatterns.after.command;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository {
    private List<Command> commandList = new ArrayList<>();

    public void add(Command command) {
        commandList.add(command);
    }

    public void removeLast() {
        commandList.get(commandList.size() - 1).undo();
        commandList.remove(commandList.size() - 1);
    }

    public void run() {
        for (Command command : commandList) {
            command.execute();
        }
    }

    public void setCommandList(List<Command> commandList) {
        this.commandList = commandList;
    }
}

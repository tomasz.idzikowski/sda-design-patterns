package io.idziksda.designpatterns.after.command;

public class Main {
    public static void main(String[] args) {
        Worker worker = new Worker();
        CommandRepository repository = new CommandRepository();
        repository.add(new WorkerBuildFarmCommand(worker));
        repository.add(new WorkerBuildIrrigationCommand(worker));
        repository.add(new WorkerBuildMineCommand(worker));
        repository.run();

        repository.removeLast();
        repository.run();
    }
}

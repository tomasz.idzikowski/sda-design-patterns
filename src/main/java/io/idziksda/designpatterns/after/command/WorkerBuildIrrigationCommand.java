package io.idziksda.designpatterns.after.command;

public class WorkerBuildIrrigationCommand implements Command{
    private Worker worker;

    public WorkerBuildIrrigationCommand(Worker worker) {
        this.worker = worker;
    }

    @Override
    public void execute() {
        worker.buildIrrigation();
    }

    @Override
    public void undo() {
        worker.undoBuildIrrigation();
    }
}

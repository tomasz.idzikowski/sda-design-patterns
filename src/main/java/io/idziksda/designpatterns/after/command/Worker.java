package io.idziksda.designpatterns.after.command;

public class Worker {
    public void buildFarm() {
        System.out.println("Building Farm");
    }

    public void buildMine() {
        System.out.println("Building mine");
    }

    public void buildIrrigation() {
        System.out.println("Building irrigation");
    }

    public void undoBuildFarm(){
        System.out.println("Mine destroyed!");
    }

    public void undoBuildMine(){
        System.out.println("Mine destroyed!");
    }

    public void undoBuildIrrigation(){
        System.out.println("Irrigation destroyed!");
    }
}

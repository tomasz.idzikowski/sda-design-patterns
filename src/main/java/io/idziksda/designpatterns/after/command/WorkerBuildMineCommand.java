package io.idziksda.designpatterns.after.command;

public class WorkerBuildMineCommand implements Command{
    private Worker worker;

    public WorkerBuildMineCommand(Worker worker) {
        this.worker = worker;
    }

    @Override
    public void execute() {
        worker.buildMine();
    }

    @Override
    public void undo() {
        worker.undoBuildMine();
    }
}

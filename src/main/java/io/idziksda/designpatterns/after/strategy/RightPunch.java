package io.idziksda.designpatterns.after.strategy;

public class RightPunch implements Punch{
    @Override
    public void punch() {
        System.out.println("Hiting with right punch");
    }
}

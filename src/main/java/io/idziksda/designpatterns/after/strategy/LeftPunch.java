package io.idziksda.designpatterns.after.strategy;

public class LeftPunch implements Punch{
    @Override
    public void punch() {
        System.out.println("Hiting with left punch");
    }
}

package io.idziksda.designpatterns.after.chainofresponsibility;

public class DollarMoneyExchanger extends ExchangeMoney{
    private String currency="$";
    private double rate= 4.13;

    @Override
    public double exchangeMoney(double money,String currency) {
        if (this.currency.equals(currency)){
            return money/rate;
        }
        return getExchangeMoney().exchangeMoney(money,currency);
    }
}

package io.idziksda.designpatterns.after.chainofresponsibility;

public abstract class ExchangeMoney {
    private ExchangeMoney exchangeMoney;

    public abstract double exchangeMoney(double money, String currency);

    public ExchangeMoney getExchangeMoney() {
        return exchangeMoney;
    }

    public void setExchangeMoney(ExchangeMoney exchangeMoney) {
        this.exchangeMoney = exchangeMoney;
    }
}

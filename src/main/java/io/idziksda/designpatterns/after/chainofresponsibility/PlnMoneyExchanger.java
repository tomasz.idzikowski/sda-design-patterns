package io.idziksda.designpatterns.after.chainofresponsibility;

public class PlnMoneyExchanger extends ExchangeMoney{
    private String currency="PLN";
    private double rate= 4.13;

    @Override
    public double exchangeMoney(double money,String currency) {
        if (this.currency.equals(currency)){
            return money*rate;
        }
        return getExchangeMoney().exchangeMoney(money,currency);
    }
}

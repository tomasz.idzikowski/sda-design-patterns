package io.idziksda.designpatterns.after.chainofresponsibility;

public class Main {
    public static void main(String[] args) {
        double sum = 100;
        String currency = "EURO";
        ExchangeMoney plExchangeMoney = new PlnMoneyExchanger();
        ExchangeMoney dolarExchangeMoney = new DollarMoneyExchanger();
        ExchangeMoney euroExchangeMoney = new EuroMoneyExchanger();

        plExchangeMoney.setExchangeMoney(dolarExchangeMoney);
        dolarExchangeMoney.setExchangeMoney(euroExchangeMoney);

        System.out.println(plExchangeMoney.exchangeMoney(sum, currency));
    }
}

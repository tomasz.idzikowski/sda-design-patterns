package io.idziksda.designpatterns.after.chainofresponsibility;

public class EuroMoneyExchanger extends ExchangeMoney{
    private String currency="EURO";
    private double rate= 4.02;

    @Override
    public double exchangeMoney(double money,String currency) {
        if (this.currency.equals(currency)){
            return money/rate;
        }
        return -1;
    }
}

package io.idziksda.designpatterns.after.visitor;

public class Main {
    public static void main(String[] args) {
        Soldier soldier = new Soldier("Terminator", 50, "Laser Pistol");
        Tank tank = new Tank("Rudy-102", 300, "Plasma Beam");
        F16 f16 = new F16(250, "Fusion Ball");

        PublicVisitor politician = new PublicVisitor("Famous politician");
        int politicianSum = 0;
        politicianSum += soldier.invite(politician);
        politicianSum += tank.invite(politician);
        politicianSum += f16.invite(politician);

        MinisterOfDefenseVisitor minister = new MinisterOfDefenseVisitor("Minister");
        int ministerSum = 0;
        ministerSum += soldier.invite(minister);
        ministerSum += tank.invite(minister);
        ministerSum += f16.invite(minister);

        System.out.println(politician.getName() + " is visiting the army and result is " + politicianSum);
        System.out.println(minister.getName() + " is visiting the army and the result is " + ministerSum);
    }
}

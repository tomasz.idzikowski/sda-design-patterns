package io.idziksda.designpatterns.after.visitor;

public class F16 implements Unit {
    private int armor;
    private String weapon;

    public F16(int armor, String weapon) {
        this.armor = armor;
        this.weapon = weapon;
    }

    public int getArmor() {
        return armor;
    }

    public String getWeapon() {
        return weapon;
    }

    @Override
    public int invite(Visitor visitor) {
        return visitor.visit(this);
    }
}

package io.idziksda.designpatterns.after.visitor;

public interface Visitor {
    int visit(F16 f16);

    int visit(Soldier soldier);

    int visit(Tank tank);
}

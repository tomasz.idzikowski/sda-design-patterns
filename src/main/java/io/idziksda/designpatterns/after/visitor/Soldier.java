package io.idziksda.designpatterns.after.visitor;

public class Soldier implements Unit {
    private String name;
    private int health;
    private String weapon;

    public Soldier(String name, int health, String weapon) {
        this.name = name;
        this.health = health;
        this.weapon = weapon;
    }

    public String getWeapon() {
        return weapon;
    }

    public int getHealth() {
        return health;
    }

    public String getName() {
        return name;
    }

    @Override
    public int invite(Visitor visitor) {
        return visitor.visit(this);
    }
}

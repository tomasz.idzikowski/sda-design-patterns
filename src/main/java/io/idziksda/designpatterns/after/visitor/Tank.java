package io.idziksda.designpatterns.after.visitor;

public class Tank implements Unit {
    private String name;
    private int armour;
    private String weapon;

    public Tank(String name, int armour, String weapon) {
        this.name = name;
        this.armour = armour;
        this.weapon = weapon;
    }

    public String getName() {
        return name;
    }

    public String getWeapon() {
        return weapon;
    }

    public int getArmour() {
        return armour;
    }

    @Override
    public int invite(Visitor visitor) {
        return visitor.visit(this);
    }
}

package io.idziksda.designpatterns.after.singleton;

public class Main {
    public static void main(String[] args) {
        Civilization civilization1=Civilization.getGame();
        Civilization civilization2=Civilization.getGame();
        System.out.println(civilization1.equals(civilization2));
    }
}

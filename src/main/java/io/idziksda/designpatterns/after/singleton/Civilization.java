package io.idziksda.designpatterns.after.singleton;

import java.util.List;

public class Civilization {
    private static Civilization game;

    private int MAX_UNITS_NUMBER = 100;
    private List<String> availableUnits;
    private List<String> availableTerrains;

    private Civilization() {
    }

    public void runGame(){
        // game mechanism
    }

    public static Civilization getGame() {
        if (game==null) {
            game=new Civilization();
        }
        return game;
    }
}

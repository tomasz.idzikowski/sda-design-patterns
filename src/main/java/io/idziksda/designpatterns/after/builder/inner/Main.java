package io.idziksda.designpatterns.after.builder.inner;

public class Main {
    public static void main(String[] args) {
        Computer computer = new Computer.ComputerBuilder()
                .buildProcessor("Intel i5-10400F")
                .buildMemory("16 GB (2x8GB)")
                .buildGraphicCard("GeForce GTX 1650")
                .build();
    }
}

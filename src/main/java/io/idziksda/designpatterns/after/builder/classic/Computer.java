package io.idziksda.designpatterns.after.builder.classic;

public class Computer {
    private String processor;
    private String memory;
    private String graphicCard;

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public void setGraphicCard(String graphicCard) {
        this.graphicCard = graphicCard;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "processor='" + processor + '\'' +
                ", memory='" + memory + '\'' +
                ", graphicCard='" + graphicCard + '\'' +
                '}';

    }
}

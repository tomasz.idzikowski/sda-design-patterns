package io.idziksda.designpatterns.after.builder.classic;

public class ConcreteBuilder implements ComputerBuilder {
    private Computer computer;

    public ConcreteBuilder() {
        this.computer = new Computer();
    }

    @Override
    public void buildProcessor() {
        this.computer.setProcessor("Intel i5-10400F");
    }

    @Override
    public void buildMemory() {
        this.computer.setMemory("16 GB (2x8GB)");
    }

    @Override
    public void buildGraphicCard() {
        this.computer.setGraphicCard("GeForce GTX 1650");
    }

    @Override
    public Computer getComputer() {
        return computer;
    }
}

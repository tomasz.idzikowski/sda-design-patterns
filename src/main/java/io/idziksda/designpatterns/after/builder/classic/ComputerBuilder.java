package io.idziksda.designpatterns.after.builder.classic;

public interface ComputerBuilder {
    void buildProcessor();
    void buildMemory();
    void buildGraphicCard();
    Computer getComputer();
}

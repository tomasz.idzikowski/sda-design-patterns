package io.idziksda.designpatterns.after.builder.classic;

public class ComputerDirector {
    private ComputerBuilder computerBuilder;

    public ComputerDirector(ComputerBuilder computerBuilder) {
        this.computerBuilder = computerBuilder;
    }

    public void buildComputer() {
        computerBuilder.buildProcessor();
        computerBuilder.buildMemory();
        computerBuilder.buildGraphicCard();
    }

    public Computer getComputer(){
        return computerBuilder.getComputer();
    }
}

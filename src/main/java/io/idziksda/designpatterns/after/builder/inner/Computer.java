package io.idziksda.designpatterns.after.builder.inner;

public class Computer {
    private String processor;
    private String memory;
    private String graphicCard;

    private Computer(ComputerBuilder carBuilder) {
        this.processor = carBuilder.processor;
        this.memory = carBuilder.memory;
        this.graphicCard = carBuilder.graphicCard;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "processor='" + processor + '\'' +
                ", memory='" + memory + '\'' +
                ", graphicCard='" + graphicCard + '\'' +
                '}';
    }

    public static class ComputerBuilder {
        private String processor;
        private String memory;
        private String graphicCard;

        public Computer build() {
            return new Computer(this);
        }

        public ComputerBuilder buildProcessor(String processor) {
            this.processor = processor;
            return this;
        }

        public ComputerBuilder buildMemory(String memory) {
            this.memory = memory;
            return this;
        }

        public ComputerBuilder buildGraphicCard(String graphicCard) {
            this.graphicCard = graphicCard;
            return this;
        }
    }
}

package io.idziksda.designpatterns.after.builder.classic;

public class Main {
    public static void main(String[] args) {
        ConcreteBuilder concreteBuilder = new ConcreteBuilder();
        ComputerDirector director = new ComputerDirector(concreteBuilder);
        director.buildComputer();

        Computer i5 = director.getComputer();
    }
}

package io.idziksda.designpatterns.after.observer;

public class Main {
    public static void main(String[] args) {
        Player player=new Player("Lewandowski",Status.IDLE);
        TvNotificaton tv=new TvNotificaton();
        RadioNotification radio=new RadioNotification();
        player.addObserver(tv);
        player.addObserver(radio);
        player.update(Status.SCORED);
    }
}

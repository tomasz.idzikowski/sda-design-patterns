package io.idziksda.designpatterns.after.observer;

public class TvNotificaton implements Observer{

    @Override
    public void update(Player player) {
        System.out.println("Tv info : ");
        System.out.println("PlayerOriginator " + player + " changed status to " + player.getStatus());
    }
}

package io.idziksda.designpatterns.after.proxy;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        TeamService teamService =new TeamProxy();
        teamService.getPlayerValues(0);
        teamService.getPlayerValues(1);
        teamService.getPlayerValues(2);
    }
}

package io.idziksda.designpatterns.after.proxy;

import io.idziksda.designpatterns.before.proxy.Player;

import java.util.Arrays;
import java.util.List;

public class TeamServiceImpl implements TeamService {
    private List<Player> players;

    public TeamServiceImpl() throws InterruptedException {
        loadPlayersData();
    }

    private void loadPlayersData() throws InterruptedException {
        Player player1 = new Player("Lewandowski", "Polish", 40);
        Player player2 = new Player("Muller", "German", 30);
        Player player3 = new Player("Sane", "Senegal", 60);

        this.players = Arrays.asList(player1, player2, player3);
        Thread.sleep(5000);
    }

    public void getPlayerValues(int index) {
        System.out.println(players.get(index).getValue());
    }
}

package io.idziksda.designpatterns.after.proxy;

public class Player {
    private String name;
    private String nationality;
    private int value;

    public Player(String name, String nationality, int value) {
        this.name = name;
        this.nationality = nationality;
        this.value = value;
    }
}

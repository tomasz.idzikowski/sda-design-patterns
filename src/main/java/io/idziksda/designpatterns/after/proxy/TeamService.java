package io.idziksda.designpatterns.after.proxy;

public interface TeamService {
    void getPlayerValues(int index) throws InterruptedException;
}

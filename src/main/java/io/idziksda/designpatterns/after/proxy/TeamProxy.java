package io.idziksda.designpatterns.after.proxy;

public class TeamProxy implements TeamService {
    private TeamService teamService;

    public void getPlayerValues(int index) throws InterruptedException {
        if (teamService == null) {
            teamService = new TeamServiceImpl();
        }
        teamService.getPlayerValues(index);
    }
}

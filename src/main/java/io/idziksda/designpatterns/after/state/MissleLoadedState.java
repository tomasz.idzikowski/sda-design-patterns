package io.idziksda.designpatterns.after.state;

public class MissleLoadedState implements State {
    public void startTheEngine(Tank tank) {
        System.out.println("Engine already started");
    }

    public void loadTheMissle(Tank tank) {
        System.out.println("The missle already loaded");
    }

    public void getOnTarget(Tank tank) {
        tank.state = new TargetLockedState();
        System.out.println("The target locked");
    }

    public void fire(Tank tank) {
        System.out.println("You have to load the missle first");
    }
}

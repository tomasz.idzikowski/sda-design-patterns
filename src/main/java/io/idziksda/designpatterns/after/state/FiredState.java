package io.idziksda.designpatterns.after.state;

public class FiredState implements State {
    public void startTheEngine(Tank tank) {
        System.out.println("The engine already started");
    }

    public void loadTheMissle(Tank tank) {
        System.out.println("The missle already loaded");
    }

    public void getOnTarget(Tank tank) {
        System.out.println("The target already locked");
    }

    public void fire(Tank tank) {
        System.out.println("The tank already fired");
    }
}

package io.idziksda.designpatterns.after.state;

public class EngineStartedState implements State {
    public void startTheEngine(Tank tank) {
        System.out.println("Engine already started");
    }

    public void loadTheMissle(Tank tank) {
        tank.state=new MissleLoadedState();
        System.out.println("The missle loaded");
    }

    public void getOnTarget(Tank tank) {
        System.out.println("You have to load the missle first");
    }

    public void fire(Tank tank) {
        System.out.println("You have to load the missle first");
    }
}

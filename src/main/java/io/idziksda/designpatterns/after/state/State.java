package io.idziksda.designpatterns.after.state;

public interface State {
    void startTheEngine(Tank tank);

    void loadTheMissle(Tank tank);

    void getOnTarget(Tank tank);

    void fire(Tank tank);
}

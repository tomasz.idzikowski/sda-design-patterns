package io.idziksda.designpatterns.after.state;

public class Tank {
    protected State state;

    public Tank() {
        this.state = new EngineNotStartedState();
    }

    public void startTheEngine() {
        state.startTheEngine(this);
    }

    public void loadMissle() {
        state.loadTheMissle(this);
    }

    public void getOnTarget() {
        state.getOnTarget(this);
    }

    public void fire() {
        state.fire(this);
    }
}

package io.idziksda.designpatterns.after.state;


public class Main {
    public static void main(String[] args) {
        Tank tank = new Tank();
        tank.fire();
        tank.getOnTarget();
        tank.loadMissle();
        tank.startTheEngine();
        tank.loadMissle();
        tank.getOnTarget();
        tank.fire();
    }
}

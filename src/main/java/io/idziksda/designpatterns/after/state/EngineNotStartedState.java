package io.idziksda.designpatterns.after.state;

public class EngineNotStartedState implements State {
    public void startTheEngine(Tank tank) {
        tank.state=new EngineStartedState();
        System.out.println("Engine started");
    }

    public void loadTheMissle(Tank tank) {
        System.out.println("You have to start the engine first");
    }

    public void getOnTarget(Tank tank) {
        System.out.println("You have to load the missle first");
    }

    public void fire(Tank tank) {
        System.out.println("You have to lock the target first");
    }
}

package io.idziksda.designpatterns.after.factorymethod.units;

public abstract class UnitFactory {
    abstract public Alien create(String type);
}
